<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use app\models\OrderItems;
use app\models\Orders;
use yii\console\Controller;
use yii\console\ExitCode;
use yii\helpers\Console;
use yii\helpers\Json;
class OrderController extends Controller
{

    /**
     * @param $uri
     * @return int
     * @throws \yii\db\Exception
     */
    public function actionUpdateNet($uri)
    {

        if ($json = file_get_contents($uri)) {
            $orders = json_decode($json, true);
            if (json_last_error() === JSON_ERROR_NONE && isset($orders['orders'])) {
                $count = count($orders['orders']);
                $i = 0;
                $transaction = \Yii::$app->db->beginTransaction();
                try {
                    Console::startProgress($i, $count, 'Load orders');
                    foreach ($orders['orders'] as $order) {
                        $model = ($model = Orders::findOne(['real_id' => $order['real_id']])) ? $model : new Orders();
                        $model->setAttributes($order);
                        if($model->save(false)){

                            foreach ($order['items'] as $item){
                                $modelItem = ($modelItem = OrderItems::findOne(['order_id' => $model->id])) ? $modelItem : new OrderItems();
                                $modelItem->setAttributes($item);
                                $modelItem->order_id = $model->id;
                                $modelItem->save(false);
                            }
                        }

                        $i++;
                        Console::updateProgress($i, $count);
                    }
                    $transaction->commit();
                } catch (\Exception $e) {
                    $transaction->rollBack();
                    throw $e;
                }
            }
        }

        return ExitCode::OK;
    }

    /**
     * @param $file
     * @return void
     * @throws \yii\base\InvalidRouteException
     * @throws \yii\console\Exception
     */
    public function actionUpdateLocal($file){
        $path = \Yii::getAlias('@app'). DIRECTORY_SEPARATOR . $file;
        if(file_exists($path)) {
            \Yii::$app->runAction('order/update-net', [$path]);
        }
    }

    /**
     * @param $id
     * @return void
     */
    public function actionInfo($id){

        $model['order'] = Orders::findOne($id);
        if($model['order']) {
            $model['items'] = $model['order']->orderItems;
            $model['items_count'] = count($model['items']);
        }
        Console::output(Json::encode($model));

    }
}
