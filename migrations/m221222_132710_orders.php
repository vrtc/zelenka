<?php

use yii\db\Migration;

/**
 * Class m221222_132710_orders
 */
class m221222_132710_orders extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%orders}}', [
            'id' => $this->bigPrimaryKey()->unsigned(),
            'real_id' => $this->bigInteger()->unsigned()->notNull(),
            'user_name' => $this->string(255)->null(),
            'user_phone' => $this->string(255)->null(),
            'warehouse_id' => $this->integer(),
            'status' => $this->integer(),
            'is_paid' => $this->integer(1),
            'promocode' => $this->string()->null(),
            'type' => $this->integer(),
            'created_at' => $this->timestamp()->defaultValue(new \yii\db\Expression('NOW()')),
        ]);

        $this->createTable('{{%order_items}}', [
            'primary_id' => $this->bigPrimaryKey()->unsigned(),
            'id' => $this->text(),
            'order_id' => $this->bigInteger()->unsigned()->notNull(),
            'name' => $this->text()->null(),
            'manufacturer' => $this->string(255)->null(),
            'barcodes' => $this->text()->null(),
            'quantity' => $this->float()->null(),
            'price' => $this->float(),
            'amount' => $this->float()
        ]);

        $this->addForeignKey(
            'fk_order_item',
            '{{%order_items}}',
            'order_id',
            '{{%orders}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m221222_132710_orders cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m221222_132710_orders cannot be reverted.\n";

        return false;
    }
    */
}
