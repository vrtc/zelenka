<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "order_items".
 *
 * @property int $primary_id
 * @property string|null $id
 * @property int $order_id
 * @property string|null $name
 * @property string|null $manufacturer
 * @property string|null $barcodes
 * @property float|null $quantity
 * @property float|null $price
 * @property float|null $amount
 *
 * @property Orders $order
 */
class OrderItems extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'order_items';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'name', 'barcodes'], 'string'],
            [['order_id'], 'required'],
            [['order_id'], 'integer'],
            [['quantity', 'price', 'amount'], 'number'],
            [['manufacturer'], 'string', 'max' => 255],
            [['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => Orders::class, 'targetAttribute' => ['order_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'primary_id' => 'Primary ID',
            'id' => 'ID',
            'order_id' => 'Order ID',
            'name' => 'Name',
            'manufacturer' => 'Manufacturer',
            'barcodes' => 'Barcodes',
            'quantity' => 'Quantity',
            'price' => 'Price',
            'amount' => 'Amount',
        ];
    }

    /**
     * Gets query for [[Order]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Orders::class, ['id' => 'order_id']);
    }
}
