<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "orders".
 *
 * @property int $id
 * @property int $real_id
 * @property string|null $user_name
 * @property string|null $user_phone
 * @property int|null $warehouse_id
 * @property int|null $status
 * @property int|null $is_paid
 * @property string|null $promocode
 * @property int|null $type
 * @property string $created_at
 *
 * @property OrderItems[] $orderItems
 */
class Orders extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'orders';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['real_id'], 'required'],
            [['real_id', 'warehouse_id', 'status', 'is_paid', 'type'], 'integer'],
            [['created_at'], 'safe'],
            [['user_name', 'promocode'], 'string', 'max' => 255],
            [['user_phone'], 'string', 'max' => 11],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'real_id' => 'Real ID',
            'user_name' => 'User Name',
            'user_phone' => 'User Phone',
            'warehouse_id' => 'Warehouse ID',
            'status' => 'Status',
            'is_paid' => 'Is Paid',
            'promocode' => 'Promocode',
            'type' => 'Type',
            'created_at' => 'Created At',
        ];
    }

    /**
     * Gets query for [[OrderItems]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getOrderItems()
    {
        return $this->hasMany(OrderItems::class, ['order_id' => 'id']);
    }

}
